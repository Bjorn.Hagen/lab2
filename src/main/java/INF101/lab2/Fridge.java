package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    int current_items_in_fridge             = 0;
    int max_size                            = 20;
    ArrayList<FridgeItem> expireditemslist  = new ArrayList<>();
    ArrayList<FridgeItem> itemslist         = new ArrayList<>();
    ArrayList<FridgeItem> gooditemslist     = new ArrayList<>();


    @Override
    public int nItemsInFridge() {
        return itemslist.size();
    }

    @Override
    public int totalSize() {
        if (itemslist.size()<max_size) {return max_size-itemslist.size();
        }
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemslist.size()<max_size) {
            itemslist.add(item);
            return true;}
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
       
        
        if (itemslist.remove(item)) {
        } else {throw new NoSuchElementException();}
       
    }
    

    @Override
    public void emptyFridge() {
        current_items_in_fridge = 0;
        itemslist.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expireditemslist = new ArrayList<FridgeItem>();

        for (FridgeItem item : this.itemslist) {
            if (item.hasExpired()) {
                expireditemslist.add(item);
                
            }
        }
        this.itemslist.removeAll(expireditemslist);

        return expireditemslist;
            
    }   
}
